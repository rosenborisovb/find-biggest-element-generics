public class Demo {

    public static void main(String[] args){

        Rectangle[] r = {
                new Rectangle(5, 7),
                new Rectangle(9, 8),
                new Rectangle(3, 7)
        };
        Pomoshtnik<Rectangle> pomoshtnik = new BigestAresaRectangle();
        Rectangle biggestRectangle = Helpers.findBiggestElement(r, pomoshtnik);

        System.out.println(biggestRectangle.getWidth());

        Rectangle biggestParameter = Helpers.findBiggestElement(r, new Pomoshtnik<Rectangle>() {
            @Override
            public int compare(Rectangle e1, Rectangle e2) {

                double p1 = e1.getWidth() * 2 + e1.getHeight() * 2;
                double p2 = e2.getWidth() * 2 + e2.getHeight() * 2;

                if (p1 == p2){
                    return 0;
                }else if (p1 < p2){
                    return 1;
                }else {
                    return -1;
                }
            }

        });

        System.out.println(biggestParameter.getHeight());
    }

}
