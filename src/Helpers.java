public class Helpers<T> {

    public static <T> T findBiggestElement(T[] elements, Pomoshtnik pomoshtnik){

        if (elements == null || elements.length == 0){
            return null;
        }

        T biggestElement = elements[0];
        for (int i = 1; i < elements.length; i++){
            int result = pomoshtnik.compare(biggestElement, elements[i]);
            if (result == 1){
                biggestElement = elements[i];
            }
        }
        return biggestElement;

    }

}
