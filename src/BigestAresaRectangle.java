public class BigestAresaRectangle implements Pomoshtnik<Rectangle> {
    @Override
    public int compare(Rectangle e1, Rectangle e2) {
        double a1 = e1.getWidth() * e1.getHeight();
        double a2 = e2.getWidth() * e2.getHeight();

        if (a1 == a2){
            return 0;
        }else if (a1 < a2){
            return 1;
        }else {
            return -1;
        }
    }
}
