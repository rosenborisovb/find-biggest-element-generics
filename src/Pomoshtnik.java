public interface Pomoshtnik<E> {

    int compare(E e1, E e2);

}
